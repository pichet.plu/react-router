import React from 'react'
import { Switch, Route } from 'react-router-dom'
import { Content } from './'
//import { Content } from './'
//import { Signin, Signup } from '../containers'
//import Router from 'react-router-dom/Router';

const App = () => (
  <div>
    {/* <Header /> */}
    <div className="container">

      {/* <Route path='/' component={Home} /> */}

      <Route component={Content} />

      {/* <Switch>
        <Route path='/sign-in' component={Signin} />
        <Route path='/sign-up' component={Signup} />
        <Route component={Content} />
      </Switch> */}
    </div>
  </div>
)

export default App
